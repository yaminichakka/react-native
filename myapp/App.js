/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Image,
  StatusBar,
  Button
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class App extends Component
{
  constructor(props) {
    super(props)
    this.state={
      
    }
    this.buttonPressed = this.buttonPressed.bind(this)
   //this.handleChangeText = this.handleChangeText.bind(this)
   // this.state.customStyles = {
     // color:'red'
    }
   //   setInterval(()=>{
  //     if(this.state.customStyles.color == 'red')
  //     {
  //       this.setState({
  //         customStyles:{
  //           color:'blue'
  //         }
  //       })
  //     } else {
  //       this.setState({
  //         customStyles:{
  //           color:'red'
  //         }
  //       })
  //     }
      
  //   },1000)
  // }

  buttonPressed()
  {
      console.log(this.state.username, this.state.password)
      // const username = this._username.lastNativeText
      // const passord = this._password.lastNativeText
      // console.log(username,passord)
  }
  
  render(){
    return(
     <View style={styles.sectionContainer}>
       {/* <Text style={styles.sectionTitle,this.state.customStyles}>Hello World!Hi I am Yamini</Text>  */}
       <Text>Username:</Text>
       <TextInput defaultValue={this.state.username} onChangeText={text=>this.setState({username:text})}/>
       <Text>Password:</Text>
       <TextInput defaultValue={this.state.password} onChangeText={text=>this.setState({password:text})}/>
       <Button title={"Submit"} onPress={this.buttonPressed}/>
       
        {/* <View style={styles.half1}>
       <Text>Hello 1</Text>
       </View>
       <View style={styles.half2}>
       <View style={styles.half21}>
        <Text>Hello 21</Text>
        </View>
       <View style={styles.half22}>
        <Text>Hello 22</Text>
        </View>
       </View> */}
     </View>

    );
  }
}

const styles = StyleSheet.create({
  // scrollView: {
  //   backgroundColor: Colors.lighter,
  // },
  // engine: {
  //   position: 'absolute',
  //   right: 0,
  // },
  // body: {
  //   backgroundColor: Colors.white,
  // },
  sectionContainer: {
    flex:1,
    justifyContent:'center',
    padding:20
  },
  // sectionTitle: {
  //   justifyContent:'center',
  //   fontSize: 40,
  //   fontWeight: '600',
  //   color: 'green'
  // },
  // sectionDescription: {
  //   marginTop: 8,
  //   fontSize: 18,
  //   fontWeight: '400',
  //   color: Colors.dark,
  // },
  // highlight: {
  //   fontWeight: '700',
  // },
  // footer: {
  //   color: Colors.dark,
  //   fontSize: 12,
  //   fontWeight: '600',
  //   padding: 4,
  //   paddingRight: 12,
  //   textAlign: 'right',
  //  },
  // half1: {
  //   flex:1,
  //   backgroundColor:'red',
  //   justifyContent:'center'
  // },
  
  // half2: {
  //   flex:1,
  //   backgroundColor:'blue'
  // },
  // half21: {
  //   flex:1,
  //   backgroundColor:'green',
  //   justifyContent:'center'
  // },
  
  // half22: {
  //   flex:1,
  //   backgroundColor:'yellow',
  //   justifyContent:'center'
  // }
});

export default App;
